/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import modele.classesmetiers.medicaments.Dosage;
import modele.classesmetiers.medicaments.Medicament;
import modele.classesmetiers.medicaments.Presentation;
import modele.classesmetiers.medicaments.Substance;
import modele.classesmetiers.medicaments.AvisHASASMR;
import modele.classesmetiers.medicaments.AvisHASSMR;
import modele.classesmetiers.medicaments.GroupeGenerique;
import modele.classesmetiers.medicaments.LienAvis;
import modele.classesmetiers.medicaments.TypeGenerique;

/**
 *
 * @author fabrice_2
 */
public class ChargeMedicamentsFromFicCSV {
    private HashMap <String, Medicament> lesMedicaments;
    private HashMap <String, Substance> lesSubstances;
    private HashMap <String, LienAvis> lesLiens;
    private HashMap <String, TypeGenerique> lesTypesGeneriques;
    private HashMap <String, GroupeGenerique> lesGroupesGeneriques;
    private BufferedWriter ecris;
    
    public ChargeMedicamentsFromFicCSV(){
        this.chargeFicErreur();
        this.chargeMedicaments();
        this.chargePresentations();
        this.chargeSubstances();
        this.chargeLiensAvisHAS();
        this.chargeAvisHAS();
        this.chargeTypeGenerique();
        this.chargeGroupeGenerique();
        this.chargeConditionPrescription();
        this.fermeFicErreur();
        this.serialiseMedicament();
    }
    
    private void serialiseMedicament() {
        BufferedReader br = null;
       
        try {
            String fichierDest =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "fichiersSer" + File.separator + "medicaments.ser";
            ObjectOutputStream oos =  new ObjectOutputStream(new FileOutputStream(fichierDest)) ;
            oos.writeObject(this.lesMedicaments) ;
        } catch (FileNotFoundException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }      
    }
    
    /* Charge les médicaments
    */
    private void chargeMedicaments(){
        BufferedReader br = null;
        String line;
        String cvsSplitBy = "\t";
        this.lesMedicaments = new HashMap();
        try {
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "Medicaments"+ File.separator + "CIS_bdpm.txt";
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fichierSrc), "ISO-8859-1"));
            
            int nbligne = 0;
            while ((line = br.readLine()) != null) {
                String[] med = line.split(cvsSplitBy);
                
                //si le médicament est mal renseigné
                if (med.length == 12){
                    //med[7]=> date
                    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
                    Date d = formater.parse(med[7]);

                    boolean b ;
                    if (med[11].compareToIgnoreCase("oui") == 0){
                        b = true;
                    }
                    else {
                        b = false;
                    }

                    //il y a un espace au début du nom du laboratoire , on le supprime
                    String titulaire = med[10].substring(1,med[10].length()); 
                    
                    //public Medicament(String codeCIS, String denomination, String formePharma, String voieAdmin, String statutAdminAMM, String typeProcAMM, String etatCommercialisation, Date dateAMM, String statusBDM, String numAutEurop, String titulaire, boolean surveillance) {
                    Medicament m = new Medicament (med[0], med[1], med[2], med[3], med[4], 
                                        med[5], med[6], d, med[8], med[9], titulaire, b);

                    this.lesMedicaments.put(med[0], m);
                    
                    nbligne ++;
                }
                else{
                    ecris.write("ERREUR Lecture CIS_Bdmp : Le médicament à la ligne " + nbligne + "n'a pas tous les attributs renseignés \r\n");
                }          
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException | ParseException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    
    /*
        Présentation : contient la liste des présentations (boîtes de médicaments) disponibles pour les médicaments 
        Un médicament a un code CIS. Il peut être présenté de 0..n formes => c'est le code CIP de la présentation
        (code CIP = Code Identifiant de la Présentation)
    */
    private void chargePresentations(){
        BufferedReader br = null;
        String line;
        String cvsSplitBy = "\t";
        try {
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "Medicaments"+ File.separator + "CIS_CIP_bdpm.txt";
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fichierSrc), "ISO-8859-1"));
            
            while ((line = br.readLine()) != null) {
                String[] pres = line.split(cvsSplitBy);
                
                //on recherche le médicament par son numéro de CIP
                Medicament m = this.lesMedicaments.get(pres[0]);
                SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
                Date d = formater.parse(pres[5]);
                
                if (m!=null){
                    //il arrive que le fichier ne soit pas rempli (les quatre dernieres valeurs ou seulement la dernière)
                    Presentation p = null;

                    switch (pres.length ){
                        case 13 :
                            p = new Presentation (pres[1], pres[2], pres[3], pres[4], d,  
                            pres[6], pres[7], pres[8], pres[9], pres[12], m);
                            break;
                    
                        case 12:
                            p = new Presentation (pres[1], pres[2], pres[3], pres[4], d,  
                            pres[6], pres[7], pres[8], pres[10], m);

                            break;
                        case 8: 
                            p = new Presentation (pres[1], pres[2], pres[3], pres[4], d,  
                            pres[6], pres[7], m);
                            break;
                    }
                    
                    if (p!= null){
                        m.setPresentation(p);
                    }
                }
                else {
                    ecris.write("ERREUR Lecture CIS_CIP_Bdmp (présentation du médicament) : le médicament avec le CIP "+ pres[0] +" n'existe pas \r\n");
                }
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException | ParseException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    
    /**
     * Une substance = un composé dans un médicament
     * Il est possible qu'une substance apparaisse plusieurs fois dans le fichier (avec le même code)
     * On recherche la substance dans le HashMap, si elle n'existe pas, on l'insère et on l'ajoute ensuite au médicament
     */
    private void chargeSubstances(){
        BufferedReader br = null;
        String line;
        String cvsSplitBy = "\t";
        try {
            this.lesSubstances = new HashMap();
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "Medicaments"+ File.separator + "CIS_COMPO_bdpm.txt";
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fichierSrc), "ISO-8859-1"));
            int nbligne = 0;
            while ((line = br.readLine()) != null) {
                String[] subs = line.split(cvsSplitBy);

                if (subs.length == 8){
                    //on regarde si la substance existe déjà dans le tableau de substances
                    Substance s = this.lesSubstances.get(subs[2]);
                    if (s == null){
                        s = new Substance (subs[2], subs[3]);
                        this.lesSubstances.put(subs[2], s);
                    }

                    //on recherche le médicament par son numéro de CIP
                    Medicament m = this.lesMedicaments.get(subs[0]);

                    if (m!=null){
                        //  public Dosage(String designElemPharma, String dosage, String reference, String nature, String numSAFT) {
                        Dosage d = new Dosage (subs[1], subs[4], subs[5], subs[6], subs[7]);
                        m.setComposant(s, d);
                    }
                    else {
                        ecris.write("ERREUR Lecture CIS_COMPO_bdpm (substances du médicament) : le médicament avec le CIP "+ subs[0] +" n'existe pas, bizarre !\r\n");
                    }
                }
                else{
                    ecris.write("ERREUR Lecture CIS_COMPO_bdpm (substances du médicament) : la ligne " + nbligne +" du fichier comporte une erreur (composition pas complétement renseignée ?)\r\n");
                }
                nbligne++;

            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    
    private void chargeLiensAvisHAS(){
        BufferedReader br = null;
        String line;
        String cvsSplitBy = "\t";
        this.lesLiens = new HashMap();
        
        try {
            this.lesSubstances = new HashMap();
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "Medicaments"+ File.separator + "HAS_LiensPageCT_bdpm.txt";
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fichierSrc), "ISO-8859-1"));

            while ((line = br.readLine()) != null) {
                String[] avis = line.split(cvsSplitBy);

                this.lesLiens.put(avis[0], new LienAvis(avis[0], avis[1]));
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    
    private void chargeAvisHAS(){
        BufferedReader br = null;
        String line;
        String cvsSplitBy = "\t";
        
        try {
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "Medicaments"+ File.separator + "CIS_HAS_SMR_bdpm.txt";
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fichierSrc), "ISO-8859-1"));
            
            int nbligne = 1;
            while ((line = br.readLine()) != null) {
                String[] av = line.split(cvsSplitBy);
                
                //si l'avis est mal renseigné
                if (av.length == 6){
                    Medicament m = this.lesMedicaments.get(av[0]);

                    if (m!=null){
                        //med[7]=> date
                        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMdd");
                        Date d = formater.parse(av[3]);

                        LienAvis la = this.lesLiens.get(av[1]);
                        //  public AvisHAS(String codeDossier, String motifEval, Date dateAvis, String valeurSMR, String libelleSMR) {
                        AvisHASSMR avis = new AvisHASSMR (av[1], av[2], d, la, av[4], av[5]);
                        m.setAvis(avis);
                        avis.setLeMedicament(m);
                    }
                    else {
                        ecris.write("ERREUR Lecture CIS_HAS_SMR_bdpm (avis HAS) : le médicament avec le CIP "+ av[0] +" n'existe pas\r\n");
                    }
                  
                    nbligne ++;
                }
                else{
                    ecris.write("ERREUR Lecture CIS_HAS_SMR_bdpm : L'avis à la ligne " + nbligne + "n'a pas tous les attributs renseignés \r\n");
                }          
            }
            
            fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "Medicaments"+ File.separator + "CIS_HAS_ASMR_bdpm.txt";
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fichierSrc), "ISO-8859-1"));
            
            nbligne = 1;
            while ((line = br.readLine()) != null) {
                String[] av = line.split(cvsSplitBy);
                
                //si l'avis est mal renseigné
                if (av.length == 6){
                    Medicament m = this.lesMedicaments.get(av[0]);

                    if (m!=null){
                        //med[7]=> date
                        SimpleDateFormat formater = new SimpleDateFormat("yyyyMMdd");
                        Date d = formater.parse(av[3]);

                        LienAvis la = this.lesLiens.get(av[1]);
                        
                        //  public AvisHAS(String codeDossier, String motifEval, Date dateAvis, String valeurSMR, String libelleSMR) {
                        AvisHASASMR avis = new AvisHASASMR (av[1], av[2], d, la, av[4], av[5]);
                        m.setAvis(avis);
                        avis.setLeMedicament(m);
                    }
                    else {
                        ecris.write("ERREUR Lecture CIS_HAS_ASMR_bdpm (avis HAS) : le médicament avec le CIP "+ av[0] +" n'existe pas\r\n");
                    }
                  
                    nbligne ++;
                }
                else{
                    ecris.write("ERREUR Lecture CIS_HAS_ASMR_bdpm : L'avis à la ligne " + nbligne + "n'a pas tous les attributs renseignés \r\n");
                }          
            }
            
        } catch (FileNotFoundException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException | ParseException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    
    private void chargeTypeGenerique(){
        /* cf. doc sur les médicaments Type de générique, avec les valeurs suivantes : 
            o  0 : « princeps » 
            o  1 : « générique » 
            o  2 : « génériques par complémentarité posologique » 
            o  4 : « générique substituable » 
        */
        this.lesTypesGeneriques = new HashMap();
        this.lesTypesGeneriques.put("0", new TypeGenerique("0", "princeps" ));
        this.lesTypesGeneriques.put("1", new TypeGenerique("1", "générique" ));
        this.lesTypesGeneriques.put("2", new TypeGenerique("2", "génériques par complémentarité posologique" ));
        this.lesTypesGeneriques.put("4", new TypeGenerique("4", "générique substituable" ));
        
    }
   
    
    private void chargeGroupeGenerique(){
        BufferedReader br = null;
        String line;
        String cvsSplitBy = "\t";
        try {
            this.lesSubstances = new HashMap();
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "Medicaments"+ File.separator + "CIS_GENER_bdpm.txt";
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fichierSrc), "ISO-8859-1"));
            int nbligne = 1;
            
            this.lesGroupesGeneriques = new HashMap();
            while ((line = br.readLine()) != null) {
                String[] ggen = line.split(cvsSplitBy);
                
                GroupeGenerique ggtmp  = new GroupeGenerique(ggen[0], ggen[4], ggen[1], this.lesTypesGeneriques.get(ggen[3]));
                    //this.lesGroupesGeneriques.put(ggen[0], ggtmp);
                //}
                
                //on récupère le médicament 
                Medicament m = this.lesMedicaments.get(ggen[2]);
                
                if (m!=null){
                    m.setLeGenerique(ggtmp);
                    ggtmp.setLeMedicament(m);
                }
                else {
                    ecris.write("ERREUR Lecture CIS_GENER_bdpm : le médicament avec le CIP "+ ggen[2] +" n'existe pas (ligne +" + nbligne +")\r\n");
                }
                nbligne++;
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    
     private void chargeConditionPrescription(){
        BufferedReader br = null;
        String line;
        String cvsSplitBy = "\t";
        try {
            this.lesSubstances = new HashMap();
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersSrc" + File.separator + "Medicaments"+ File.separator + "CIS_CPD_bdpm.txt";
            br = new BufferedReader(new InputStreamReader(new FileInputStream(fichierSrc), "ISO-8859-1"));
            int nbligne = 1;
            
            while ((line = br.readLine()) != null) {
                String[] pres = line.split(cvsSplitBy);
            
                //on récupère le médicament 
                Medicament m = this.lesMedicaments.get(pres[0]);

                if (m!=null){
                    m.setConditionPrescription(pres[1]);
                }
                else {
                    ecris.write("ERREUR Lecture CIS_CPD_bdpm : le médicament avec le CIP "+ pres[0] +" n'existe pas (ligne +" + nbligne +")\r\n");
                }
                nbligne++;
            }
        } catch (FileNotFoundException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }
    
     /**
      * Transforme HashMap en ArrayList
      * @return 
      */
    public ArrayList<Medicament> getLesMedicaments(){
        ArrayList<Medicament> lesMedics = new ArrayList();
        this.lesMedicaments.forEach((k,v)->{
            lesMedics.add(v);
        });
       return lesMedics;
    }
    
    /* Ouvre un fichier d'erreur dont le nom est l'heure
    actuelle -> trace de la lecteur des CSV
    */
    private void chargeFicErreur(){
        String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        
        //String s = String.valueOf(jour) + "-" + String.valueOf(mois) + "-" + String.valueOf(annee) + "_" + String.valueOf(cal.get(Calendar.MILLISECOND));
        String chemin = System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "SQLite-Medicaments" +
                File.separator + "log" +  File.separator + timeLog + ".txt";//"log_err_chargement_"+ s + ".txt";;
        File ficErr = new File(chemin);
        
        try {
            ecris = new BufferedWriter(new FileWriter(ficErr));
        } catch (IOException ex) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void fermeFicErreur(){
        try {
            ecris.close();
        } catch (IOException ex) {
            Logger.getLogger(ChargeMedicamentsFromFicCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void afficheMedicaments(){
        this.lesMedicaments.forEach((k,v)->{
            System.out.println(v);
            System.out.println();
        });
    }
    
    private void afficheUnMedicament(String numCIP){
            System.out.println(this.lesMedicaments.get(numCIP));
    }
    
}

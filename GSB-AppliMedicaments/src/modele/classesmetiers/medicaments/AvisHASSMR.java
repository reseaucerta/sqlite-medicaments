/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.classesmetiers.medicaments;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author fabrice_2
 */
public class AvisHASSMR extends AvisHAS implements Serializable {
    /*
-  Valeur du SMR 
-  Libellé du SMR */

    private String valeurSMR;
    private String libelleSMR;

    public AvisHASSMR(String codeDossier, String motifEval, Date dateAvis, String valeurSMR, String libelleSMR) {
        super (codeDossier, motifEval, dateAvis);
        this.valeurSMR = valeurSMR;
        this.libelleSMR = libelleSMR;
    }
    
    public AvisHASSMR(String codeDossier, String motifEval, Date dateAvis, LienAvis leLien, String valeurSMR, String libelleSMR) {
        super (codeDossier, motifEval, dateAvis, leLien);
        this.valeurSMR = valeurSMR;
        this.libelleSMR = libelleSMR;
    }
    
    public String getValeurSMR() {
        return valeurSMR;
    }

    public void setValeurSMR(String valeurSMR) {
        this.valeurSMR = valeurSMR;
    }

    public String getLibelleSMR() {
        return libelleSMR;
    }

    public void setLibelleSMR(String libelleSMR) {
        this.libelleSMR = libelleSMR;
    }

    @Override
    public String toString() {
        return "AvisHASSMR{" + super.toString() + " valeurSMR=" + valeurSMR + ", libelleSMR=" + libelleSMR + '}';
    }


}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.classesmetiers.medicaments;

import java.io.Serializable;

/**
 *
 * @author fabrice_2
 */
public class GroupeGenerique implements Serializable {
    /*
La clé du groupe générique est le couple (identifiant, numéro d'ordre)
-  Identifiant du groupe générique 
-  Numéro d'ordre 
-  Libellé du groupe générique 
-  Code CIS 
-  Type de générique, avec les valeurs suivantes : 
o  0 : « princeps » 
o  1 : « générique » 
o  2 : « génériques par complémentarité posologique » 
o  4 : « générique substituable » 
-  Numéro permettant de trier les éléments d’un groupe
    */
    private String identifiant;
    private String numeroOrdre;
    private String libelle;
    private TypeGenerique leType;
    private Medicament leMedicament;

    
    public GroupeGenerique(String identifiant, String numeroOrdre, String libelle, TypeGenerique leType, Medicament leMedicament) {
        this.identifiant = identifiant;
        this.numeroOrdre = numeroOrdre;
        this.libelle = libelle;
        this.leType = leType;
        this.leMedicament = leMedicament;
    }

    public GroupeGenerique(String identifiant, String numeroOrdre, String libelle, TypeGenerique leType) {
        this.identifiant = identifiant;
        this.numeroOrdre = numeroOrdre;
        this.libelle = libelle;
        this.leType = leType;
        this.leMedicament = null;
    }
    
    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getNumeroOrdre() {
        return numeroOrdre;
    }

    public void setNumeroOrdre(String numeroOrdre) {
        this.numeroOrdre = numeroOrdre;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public TypeGenerique getLeType() {
        return leType;
    }

    public void setLeType(TypeGenerique leType) {
        this.leType = leType;
    }

    public Medicament getLeMedicament() {
        return leMedicament;
    }

    public void setLeMedicament(Medicament leMedicament) {
        this.leMedicament = leMedicament;
    }


    @Override
    public String toString() {
        return "GroupeGenerique{" + "identifiant=" + identifiant + ", numeroOrdre=" + numeroOrdre + ", libelle=" + libelle + ", leType=" + leType +  '}';
    }
    
    
    
}

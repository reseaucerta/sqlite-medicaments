/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.classesmetiers.medicaments;

import java.io.Serializable;

/**
 *
 * @author fabrice_2
 */
public class LienAvis implements Serializable {
    /*Code de dossier HAS 
    Lien vers les pages d’avis de la CT */
    
    private String codeDossierHAS;
    private String lienAvisCT;

    public LienAvis(String codeDossierHAS, String lienAvisCT) {
        this.codeDossierHAS = codeDossierHAS;
        this.lienAvisCT = lienAvisCT;
    }

    public String getCodeDossierHAS() {
        return codeDossierHAS;
    }

    public void setCodeDossierHAS(String codeDossierHAS) {
        this.codeDossierHAS = codeDossierHAS;
    }

    public String getLienAvisCT() {
        return lienAvisCT;
    }

    public void setLienAvisCT(String lienAvisCT) {
        this.lienAvisCT = lienAvisCT;
    }

    @Override
    public String toString() {
        return "LienAvis{" + "codeDossierHAS=" + codeDossierHAS + ", lienAvisCT=" + lienAvisCT + '}';
    }
    
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import modele.classesmetiers.medicaments.AvisHAS;
import modele.classesmetiers.medicaments.AvisHASASMR;
import modele.classesmetiers.medicaments.AvisHASSMR;
import modele.classesmetiers.medicaments.Dosage;
import modele.classesmetiers.medicaments.GroupeGenerique;
import modele.classesmetiers.medicaments.Medicament;
import modele.classesmetiers.medicaments.Substance;

/**
 *
 * @author Fabrice Missonnier 
 */
public class GenereMedicamentSQLite {
     private HashMap<String, Medicament> lesMedicaments;

    public GenereMedicamentSQLite() {
        this.deserialiseMedicaments();
        
        this.chargeSchema();    
        this.chargeMedicaments();
        this.chargePresentations();
        this.chargeDosagesEtSubstances();
        this.chargeGroupesEtTypesGeneriques();
        this.chargeAvisEtLiensAvis();
    }
     
     private void chargeSchema() {
        try {
            String fichierDest =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "SQLite-Medicaments" + File.separator + "schema.sql";
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fichierDest), "UTF8"));
                  
            out.write("CREATE TABLE Medicament ( \ncodeCIS TEXT, \n"
                    + "denomination TEXT,\n"
                    + "formePharma TEXT, \n"
                    + "voieAdmin TEXT, \n"
                    + "statutAdminAMM TEXT, \n"
                    + "typeProcAMM TEXT, \n"
                    + "etatCommercialisation TEXT, \n"
                    + "dateAMM TEXT, \n"
                    + "statusBDM TEXT, \n"
                    + "numAutEurop TEXT, \n"
                    + "titulaire TEXT, \n"
                    + "surveillance TEXT, \n"
                    + "conditionPrescription TEXT, \n"
                    + "PRIMARY KEY (codeCIS));\n\n");
            
            out.write("CREATE TABLE Substance ( \ncodeS TEXT, \n"
                    + "denomination TEXT,\n"
                    + "PRIMARY KEY (codeS));\n\n");
            
            out.write("CREATE TABLE Presentation ( \ncodeCIP7 TEXT, \n"
                    + "libellePres TEXT,\n"
                    + "statutPres TEXT, \n"
                    + "etatCommercialisation TEXT, \n"
                    + "dateCommercialisation TEXT, \n"
                    + "codeCIP13 TEXT, \n"
                    + "agrementCollectivites TEXT, \n"
                    + "tauxRbt TEXT, \n"
                    + "prixEuro TEXT, \n"
                    + "texteRbt TEXT, \n"
                    + "codeCIS TEXT, \n"
                    + "PRIMARY KEY (codeCIP7),\n"
                    + "FOREIGN KEY (codeCIS) REFERENCES Medicament(codeCIS));\n\n");
            
            out.write("CREATE TABLE TypeGenerique ( \ncodeTG INTEGER, \n"
                    + "libelleTG TEXT,\n"
                    + "PRIMARY KEY (codeTG));\n\n");
            
            out.write("CREATE TABLE GroupeGenerique ( \nidentifiant TEXT, \n"
                    + "numeroOrdre TEXT,\n"
                    + "libelle TEXT,\n"
                    + "codeTG TEXT,\n" 
                    + "codeCIS TEXT,\n" 
                    + "PRIMARY KEY (identifiant, numeroOrdre),\n"
                    + "FOREIGN KEY (codeCIS) REFERENCES Medicament(codeCIS),\n"
                    + "FOREIGN KEY (codeTG) REFERENCES TypeGenerique(codeTG));\n\n");
             
            out.write("CREATE TABLE Dosage ( \ncodeCIS TEXT, \n"
                    + "codeS TEXT,\n"
                    + "designElemPharma TEXT,\n" 
                    + "dosage TEXT,\n" 
                    + "reference TEXT,\n" 
                    + "nature TEXT,\n" 
                    + "numSAFT TEXT,\n" 
                    + "PRIMARY KEY (codeCIS, codeS)"
                    + "FOREIGN KEY (codeCIS) REFERENCES Medicament(codeCIS), \n" 
                    + "FOREIGN KEY (codeS) REFERENCES Substance(codeS));\n\n");
             
            out.write("CREATE TABLE AvisHAS ( \ncodeDossierHAS TEXT, \n"
                    + "motifEval TEXT,\n"
                    + "dateAvis TEXT,\n" 
                    + "codeCIS TEXT,\n" 
                    + "asmrVSsmr TEXT,\n" 
                    + "valeurASMR TEXT,\n" 
                    + "libelleASMR TEXT,\n" 
                    + "valeurSMR TEXT,\n" 
                    + "libelleSMR TEXT,\n" 
                    + "lienAvisCT TEXT,\n" 
                    + "PRIMARY KEY (codeDossierHAS),\n"
                    + "FOREIGN KEY (codeCIS) REFERENCES Medicament(codeCIS));\n\n");
            

            out.close();
        } catch (IOException ex) {
            Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
    private void deserialiseMedicaments(){
        BufferedReader br = null;
        String line;
      
        try {
            String fichierSrc =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "fichiersSer" + File.separator + "medicaments.ser";
            FileInputStream fichier = new FileInputStream(fichierSrc);
            ObjectInputStream ois =  new ObjectInputStream(fichier) ;

            //deserialisation du HashMap des professionnels de santé (le HashMap est sérialisé en profondeur et contient les structures, professions, départements et les 
            //régions
            this.lesMedicaments = (HashMap<String,Medicament>)ois.readObject();
            
        } catch (FileNotFoundException e) {
            Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException | ClassNotFoundException e) {
            Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        }
    }    
    
    private void chargeMedicaments(){
        try {
            String fichierDest =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "SQLite-Medicaments" + File.separator + "medicaments.sql";
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fichierDest), "UTF8"));
             
            this.lesMedicaments.forEach((k,v)->{
              /*out.write("CREATE TABLE Medicament ( \n codeCIS v, \n"
                    + "denomination TEXT,\n"
                    + "formePharma TEXT, \n"
                    + "voieAdmin TEXT, \n"
                    + "statutAdminAMM TEXT, \n"
                    + "typeProcAMM TEXT, \n"
                    + "etatCommercialisation TEXT, \n"
                    + "dateAMM TEXT, \n"
                    + "statusBDM TEXT, \n"
                    + "numAutEurop TEXT, \n"
                    + "titulaire TEXT, \n"
                    + "surveillance TEXT, \n"
                    + "conditionPrescription TEXT, \n"
                    + "\nPRIMARY KEY (codeCIS));\n\n");
            */
                try {
                    
                    Date date = v.getDateAMM();
                    SimpleDateFormat in = new SimpleDateFormat("YYYY-MM-dd HH:MM:SS.SSS");
                    
                    out.write("INSERT INTO Medicament VALUES (\""+ v.getCodeCIS() + "\", \""+ v.getDenomination().replaceAll("\"", "") + "\", \"" + v.getFormePharma() +
                            "\", \"" + v.getVoieAdmin() + "\", \"" + v.getStatutAdminAMM() + "\", \""+ v.getTypeProcAMM() + "\", \"" + 
                            v.getEtatCommercialisation() + "\", \"" + in.format(date) + "\", \""+ v.getStatusBDM()+ "\", \"" + 
                            v.getNumAutEurop()+ "\", \"" + v.getTitulaire() + "\", \""+ v.getSurveillance()+ "\", \"" + v.getConditionPrescription()+ "\");\n");  
                    
                } catch (Exception ex) {
                    Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, ex);
                }
               
                
            });
                
            out.close(); 
        } catch (FileNotFoundException e) {
            Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    /* Un medicament a 1..* présentations, une présentation 1..1 médicament
    on tourne sur tous les médicaments, puis toutes les présentations pour chaque 
    médicament.
    */
    private void chargePresentations(){
        try {
            String fichierDest =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "SQLite-Medicaments" + File.separator + "presentations.sql";
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fichierDest), "UTF8"));
             
            this.lesMedicaments.forEach((km,vm)->{
                /*out.write("CREATE TABLE Presentation ( \ncodeCIP7 TEXT, \n"
                + "libellePres TEXT,\n"
                + "statutPres TEXT, \n"
                + "etatCommercialisation TEXT, \n"
                + "dateCommercialisation TEXT, \n"
                + "codeCIP13 TEXT, \n"
                + "agrementCollectivites TEXT, \n"
                + "tauxRbt TEXT, \n"
                + "prixEuro TEXT, \n"
                + "texteRbt TEXT, \n"
                + "codeCIS TEXT, \n"
                + "PRIMARY KEY (codeCIP7),\n"
                + "FOREIGN KEY (codeCIS) REFERENCES Medicament(codeCIS);\n\n");
                 */
                vm.getLesPresentations().stream().forEach((vp) -> {
                    try {
                        Date date = vp.getDateCommercialisation();
                        SimpleDateFormat in = new SimpleDateFormat("YYYY-MM-dd HH:MM:SS.SSS");
                        out.write("INSERT INTO Presentation VALUES (\""+ vp.getCodeCIP7() + "\", \""+ vp.getLibellePres().replaceAll("\"", "") + "\", \"" + vp.getStatutPres() +
                                "\", \"" + vp.getEtatCommercialisation()+ "\", \"" + in.format(date) + "\", \""+ vp.getCodeCIP13()+ "\", \"" +
                                vp.getAgrementCollectivites()+ "\", \"" + vp.getTauxRbt()+ "\", \""+ vp.getPrixEuro()+ "\", \"" +
                                vp.getTexteRbt().replaceAll("\"", "'")+ "\", \"" + vp.getLeMedicament().getCodeCIS()+ "\");\n");
                        
                    } catch (Exception ex) {
                        Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
            });
                
            out.close(); 
        } catch (FileNotFoundException e) {
            Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, e);
        }
   
    }
    
    
    private void chargeDosagesEtSubstances(){
        HashMap <String, Substance> lesSubstances = new HashMap();
        try {
            String fichierDest =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "SQLite-Medicaments" + File.separator + "dosages.sql";
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fichierDest), "UTF8"));
           
            this.lesMedicaments.forEach((km,vm)->{
                //on récup les compositions d'un médicament
                HashMap <Substance, Dosage> lesCompositions = vm.getComposition();
                
                // pour chaque composition
                lesCompositions.forEach((kc,vc)->{
                    /*out.write("CREATE TABLE Dosage ( \ncodeCIS TEXT, \n"
                       + "codeS TEXT,\n"
                       + "designElemPharma TEXT,\n" 
                       + "dosage TEXT,\n" 
                       + "reference TEXT,\n" 
                       + "nature TEXT,\n" 
                       + "numSAFT TEXT,\n" 
                       + "PRIMARY KEY (codeCIS, codeS)"
                       + "FOREIGN KEY (codeCIS) REFERENCES Medicament(codeCIS), \n" 
                       + "FOREIGN KEY (codeS) REFERENCES Substance(codeS);\n\n");-**/

                    try {
                        out.write("INSERT INTO Dosage VALUES (\""+ vm.getCodeCIS() + "\", \""+ kc.getCode() + "\", \"" + vc.getDesignElemPharma() +
                                "\", \"" + vc.getDosage() + "\", \"" + vc.getReference() + "\", \""+ vc.getNature() + "\", \"" +
                                vc.getNumSAFT() + "\");\n");

                    } catch (Exception ex) {
                        Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    //on ajoute la Substance au HashMap temporaire, pour pouvoir construire ensuite la table Substance
                    lesSubstances.put(kc.getCode(), kc);
                 });                    
            });
           
            out.close(); 
            
            String fichierDest2 =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "SQLite-Medicaments" + File.separator + "substances.sql";
            BufferedWriter out2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fichierDest2), "UTF8"));
            //On génère les substances
            lesSubstances.forEach((ks,vs)->{
               /* out.write("CREATE TABLE Substance ( \ncodeS TEXT, \n"
                        + "denomination TEXT,\n"
                        + "PRIMARY KEY (codeS));\n\n");*/
                
                try {
                    out2.write("INSERT INTO Substance VALUES (\""+ vs.getCode() + "\", \""+ vs.getDenomination() + "\");\n");
                } catch (Exception ex) {
                    Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            out2.close(); 
            
        } catch (FileNotFoundException e) {
            Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, e);
        }
   
    }
    
    private void chargeGroupesEtTypesGeneriques(){
        HashMap <String, GroupeGenerique> lesGroupes = new HashMap();
        
        try {
            String fichierDest =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "SQLite-Medicaments" + File.separator + "typesgeneriques.sql";
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fichierDest), "UTF8"));
           
            //les types génériques sont codés en dur ici
            //c'est moche mais j'assume
            try {
                out.write("INSERT INTO TypeGenerique VALUES (0, \"princeps\");\n");
                out.write("INSERT INTO TypeGenerique VALUES (1, \"générique\");\n");
                out.write("INSERT INTO TypeGenerique VALUES (2, \"génériques par complémentarité posologique\");\n");
                out.write("INSERT INTO TypeGenerique VALUES (4, \"générique substituable\");\n");     
            } catch (Exception ex) {
                Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, ex);
            }
           
            out.close(); 

            this.lesMedicaments.forEach((km,vm)->{
                //on récup les groupes génériques d'un médicament
                ArrayList<GroupeGenerique> lesGG = vm.getLesGeneriques() ;
                
                // pour chaque composition, on insère dans le hashmap le groupe
                //générique dont la clé est la concaténation de identifiant et numéro d'ordre
                lesGG.forEach((vgg)->{
                    lesGroupes.put(vgg.getIdentifiant()+vgg.getNumeroOrdre(), vgg);
                });                    
            });
            
            String fichierDest2 =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "SQLite-Medicaments" + File.separator + "groupesgeneriques.sql";
            BufferedWriter out2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fichierDest2), "UTF8"));
            //On génère les substances
            lesGroupes.forEach((kg,vg)->{
                /*out.write("CREATE TABLE GroupeGenerique ( \nidentifiant TEXT, \n"
                    + "numeroOrdre TEXT,\n"
                    + "libelle TEXT,\n"
                    + "codeTG TEXT,\n" 
                    + "codeCIS TEXT,\n" 
                    + "PRIMARY KEY (identifiant, numeroOrdre)"
                    + "FOREIGN KEY (codeCIS) REFERENCES Medicament(codeCIS),"
                    + "FOREIGN KEY (codeTG) REFERENCES TypeGenerique(codeTG));\n\n");*/
                try {

                    if (vg.getLeType() != null){
                        out2.write("INSERT INTO GroupeGenerique VALUES (\""+ vg.getIdentifiant() + "\", \""+ vg.getNumeroOrdre()+
                             "\", \""+ vg.getLibelle() + "\", \""+ vg.getLeType().getCodeTG() + "\", \"" + vg.getLeMedicament().getCodeCIS()+ "\");\n");
                    }
                    else {
                         out2.write("INSERT INTO GroupeGenerique VALUES (\""+ vg.getIdentifiant() + "\", \""+ vg.getNumeroOrdre()+
                             "\", \""+ vg.getLibelle() + "\", null, \"" + vg.getLeMedicament().getCodeCIS()+ "\");\n");
                    }
                        
                } catch (Exception ex) {
                    Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            out2.close(); 
            
        } catch (FileNotFoundException e) {
            Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    private void chargeAvisEtLiensAvis(){
        HashMap<String, AvisHAS> lesAvis = new HashMap();
        try {
            this.lesMedicaments.forEach((km,vm)->{
                //on récup les avis  génériques d'un médicament
                ArrayList<AvisHAS> avisMedicament = vm.getLesAvis() ;
                
                // pour chaque composition, on insère dans le hashmap le groupe
                //générique dont la clé est le code de dossier
                avisMedicament.forEach((vgg)->{
                    lesAvis.put(vgg.getCodeDossier(), vgg);
                });                    
            });
            
            String fichierDest =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "SQLite-Medicaments" + File.separator + "avis.sql";
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fichierDest), "UTF8"));
            
            //On génère les avis
            lesAvis.forEach((ka,va)->{
               
                try {
                    /*out.write("CREATE TABLE AvisHAS ( \ncodeDossierHAS TEXT, \n"
                    + "motifEval TEXT,\n"
                    + "dateAvis TEXT,\n" 
                    + "codeCIS TEXT,\n" 
                    + "asmrVSsmr TEXT,\n" 
                    + "valeurASMR TEXT,\n" 
                    + "libelleASMR TEXT,\n" 
                    + "valeurSMR TEXT,\n" 
                    + "libelleSMR TEXT,\n" 
                    + "lienAvisCT TEXT,\n" 
                    + "PRIMARY KEY (codeDossier)"
                    + "FOREIGN KEY (codeCIS) REFERENCES Medicament(codeCIS);\n\n");
            
                    */
                    Date date = va.getDateAvis();
                    SimpleDateFormat in = new SimpleDateFormat("YYYY-MM-dd HH:MM:SS.SSS");
                    
                    if (va.getClass().getName().contains("AvisHASASMR") == true){
                        AvisHASASMR avis = (AvisHASASMR)va;
                        if(va.getLeLien() != null){
                            out.write("INSERT INTO AvisHAS VALUES (\""+ va.getCodeDossier()+ "\", \""+ va.getMotifEval().replaceAll("\"", "'") + "\", \"" + in.format(date) +
                                "\", \"" + va.getLeMedicament().getCodeCIS() + "\", \"asmr\", \""+ avis.getValeurASMR() + "\", \"" +
                                avis.getLibelleASMR().replaceAll("\"", "'") + "\", \"\", \"\", \"" + va.getLeLien().getLienAvisCT() +"\");\n");     
                        }
                        else {
                            out.write("INSERT INTO AvisHAS VALUES (\""+ va.getCodeDossier()+ "\", \""+ va.getMotifEval().replaceAll("\"", "'") + "\", \"" + in.format(date) +
                                "\", \"" + va.getLeMedicament().getCodeCIS() + "\", \"asmr\", \""+ avis.getValeurASMR() + "\", \"" +
                                avis.getLibelleASMR().replaceAll("\"", "'") + "\", \"\", \"\", null);\n");     
                        }
                    }else if (va.getClass().getName().contains("AvisHASSMR") == true){
                        AvisHASSMR avis = (AvisHASSMR)va;
                        if(va.getLeLien() != null){
                            out.write("INSERT INTO AvisHAS VALUES (\""+ va.getCodeDossier()+ "\", \""+ va.getMotifEval().replaceAll("\"", "'") + "\", \"" + in.format(date) +
                                "\", \"" + va.getLeMedicament().getCodeCIS() + "\", \"smr\", \"\", \"\", \""+ avis.getValeurSMR().replaceAll("\"", "'")+ "\", \"" +
                                avis.getLibelleSMR().replaceAll("\"", "'") +"\", \"" + va.getLeLien().getLienAvisCT() +"\");\n");
                        }else {
                             out.write("INSERT INTO AvisHAS VALUES (\""+ va.getCodeDossier()+ "\", \""+ va.getMotifEval().replaceAll("\"", "'") + "\", \"" + in.format(date) +
                                "\", \"" + va.getLeMedicament().getCodeCIS() + "\", \"smr\", \"\", \"\", \""+ avis.getValeurSMR().replaceAll("\"", "'")+ "\", \"" +
                                avis.getLibelleSMR().replaceAll("\"", "'") + "\", null);\n");  
                        }
                    }   
                    
                } catch (Exception ex) {
                    Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            out.close(); 
            
        } catch (FileNotFoundException e) {
            Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    
    private void toJson(){
         try {
            String fichierDest =  System.getProperty("user.dir") + File.separator + "fichiersDest" + File.separator + "SQLite-Medicaments" + File.separator + "medicaments.json";
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fichierDest), "UTF8"));
             
            this.lesMedicaments.forEach((k,v)->{
            
                try {
                    out.write(new Gson().toJson(v)+"\n");  
                    
                } catch (Exception ex) {
                    Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, ex);
                }
              
            });
                
            out.close(); 
        } catch (FileNotFoundException e) {
            Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            Logger.getLogger(GenereMedicamentSQLite.class.getName()).log(Level.SEVERE, null, e);
        }

    }
    
}

#!/bin/bash
wget http://base-donnees-publique.medicaments.gouv.fr/telechargement.php?fichier=CIS_bdpm.txt > CIS_bdpm.txt
wget http://base-donnees-publique.medicaments.gouv.fr/telechargement.php?fichier=CIS_CIP_bdpm.txt > CIS_CIP_bdpm.txt
wget http://base-donnees-publique.medicaments.gouv.fr/telechargement.php?fichier=CIS_COMPO_bdpm.txt > CIS_COMPO_bdpm.txt
wget http://base-donnees-publique.medicaments.gouv.fr/telechargement.php?fichier=CIS_HAS_SMR_bdpm.txt > CIS_HAS_SMR_bdpm.txt
wget http://base-donnees-publique.medicaments.gouv.fr/telechargement.php?fichier=CIS_HAS_ASMR_bdpm.txt > CIS_HAS_ASMR_bdpm.txt
wget http://base-donnees-publique.medicaments.gouv.fr/telechargement.php?fichier=HAS_LiensPageCT_bdpm.txt > HAS_LiensPageCT_bdpm.txt
wget http://base-donnees-publique.medicaments.gouv.fr/telechargement.php?fichier=CIS_GENER_bdpm.txt > CIS_GENER_bdpm.txt
wget http://base-donnees-publique.medicaments.gouv.fr/telechargement.php?fichier=CIS_CPD_bdpm.txt > CIS_CPD_bdpm.txt

mv telechargement.php?fichier=CIS_bdpm.txt CIS_bdpm.txt
mv telechargement.php?fichier=CIS_CIP_bdpm.txt CIS_CIP_bdpm.txt
mv telechargement.php?fichier=CIS_COMPO_bdpm.txt CIS_COMPO_bdpm.txt
mv telechargement.php?fichier=CIS_HAS_SMR_bdpm.txt CIS_HAS_SMR_bdpm.txt
mv telechargement.php?fichier=CIS_HAS_ASMR_bdpm.txt CIS_HAS_ASMR_bdpm.txt
mv telechargement.php?fichier=HAS_LiensPageCT_bdpm.txt HAS_LiensPageCT_bdpm.txt
mv telechargement.php?fichier=CIS_GENER_bdpm.txt CIS_GENER_bdpm.txt
mv telechargement.php?fichier=CIS_CPD_bdpm.txt CIS_CPD_bdpm.txt
